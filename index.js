console.log("Mabuhay!");

// JS-is a loosely type Programming Language hindi maselan sa space, line

alert("Hello Again");

console. log (   "   Hello World!    ")   ;


console.
log

(
	"Hello World!")


	;

/*
1. single-line comment -ctrl + /
2. multi line comment - ctrl + shift + /
*/

// [SECTION] -Variables
// used to contain data
// usually stored in a computer's memory (RAM).

// declaration of variable: numbers or letters

// syntax --> let variableName;
let myVariable;
console.log(myVariable);

// syntax --> let variableValue;
let mySecondVariable = 2;
console.log(mySecondVariable);
let productName = "desktop computer";
console.log(productName);

// Reassigning value
let friend;
friend = "Kate";
friend = "Jane";
friend = "Cely";
console.log(friend)

// Syntax const variableName variableValue
const pet = "Bruno";
// pet = "Lala";
console.log(pet);

// const hoursPerDay = 24; - constant, hindi napapalitan

// Local and Global Variables

let outerVariable = "hello"; 
{
	let innerVariable = "Hello World!"
	console.log(innerVariable);
}
console.log(outerVariable);

const outerExample = "Global Variable";
{
	const innerExample = "Local variable";
	console.log(innerExample);
}
console.log(outerExample);


// Multiple Declaration
let productCode = "DC017",productBrand = "Dell";
console.log(productCode,productBrand);

// [SECTION] Data Type

// Strings -series of characters that create a word...
let country = 'Philippines';
let provice = "Metro Manila";
// 1. Contatenating Strings - uses + symbol
let fullAddress = provice + ", " + country;
console.log(fullAddress);

let greeting = "I live in" + ", " + country;
console.log(greeting);

// ESCAPE CHARACTER -\n-break
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// using double quotes
let message = "John's employees went home early";
message = 'John\'s employees went home early';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers or fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining strings and integers/numbers
console.log("My grade last sem is " + grade);

// Bollean store values 
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried:" + isMarried);
console.log("inGoodConduct:" + inGoodConduct);

// Arrays - store similar data types
// let english = 90;
// let math = 100;
// let arts = 98;

let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

// Array with different data types
let details = ["John","Smith",32,true];
console.log(details);

// Objects-used to mimic real world objects/items
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789","0999999999"],
	address: {  
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);


// Re-assigning
const anime = ["one piece", "one punch man", "Attack on Titan"];
anime[1] = ["Kimetsu no Yaiba"];
console.log(anime);
